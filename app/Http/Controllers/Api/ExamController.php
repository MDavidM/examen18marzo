<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{
    public function show($id)
    {
        return \App\Exam::with('questions')->findOrFail($id);
    }
}
