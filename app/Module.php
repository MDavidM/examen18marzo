<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
     public function exam()
    {
        return $this->hasMany(Exam::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}
