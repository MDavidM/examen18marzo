<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

     public function user()
    {
        return $this->belongsTo(User::class);
    }
}
