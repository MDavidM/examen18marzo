@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de exámenes</h1>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Title</th>
            <th>Date</th>
            <th>Materia</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($exams as $exam)
          <tr>
            <td>{{ $exam->title }}</td>
            <td>{{ $exam->date }}</td>
            <td>{{ $exam->module->name }}</td>
            <td>{{ $exam->user->name }}</td>
            <td>

              <form method="post" action="/exams/{{ $exam->id }}">
                @can ('update', $exam)
                @endcan

                <a class="btn btn-danger"  role="button"
                href="/exams/{{ $exam->id }}/delete">
                  Borrar
                </a>

              <!--{{ csrf_field() }}-->
              <input type="hidden" name="_method" value="DELETE">
              @can ('delete', $exam)
                <input type="submit" value="borrar" class="btn btn-danger">
              @endcan
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay exámenes!!</td></tr>
        @endforelse
      </tbody>
    </table>

    {{ $exams->render() }}
  </div>
</div>
</div>
@endsection
