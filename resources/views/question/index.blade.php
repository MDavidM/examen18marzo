@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de preguntas</h1>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Enunciado</th>
            <th>A</th>
            <th>B</th>
            <th>C</th>
            <th>D</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($questions as $question)
          <tr>
            <td>{{ $question->text }}</td>
            <td>{{ $question->a }}</td>
            <td>{{ $question->b }}</td>
            <td>{{ $question->c }}</td>
            <td>{{ $question->d }}</td>
            <td>

              <form method="post" action="/questions/{{ $question->id }}">
                @can ('update', $question)
                <a class="btn btn-primary"  role="button"
                href="/questions/{{ $question->id }}/edit">
                  Editar
                </a>
                @endcan
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              @can ('delete', $question)
                <input type="submit" value="borrar" class="btn btn-danger">
              @endcan
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay preguntas!!</td></tr>
        @endforelse
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection
